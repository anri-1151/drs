package com.creative.merge.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.creative.merge.model.User;
import com.creative.merge.utils.PaginationAndFullSearchQuery;

@Repository
public class UserDAO extends PaginationAndFullSearchQuery {

    @PersistenceContext
    private EntityManager em;
    
    public User saveUser(User user) {
		if(user.getId() != null) {
			em.merge(user);
		} else {
			em.persist(user);
		}
		return user;
	}
    
    public void delete(Long id) {
		User user = em.find(User.class, id);
		em.remove(user);
	}

	@SuppressWarnings("unchecked")
	public User findByUserName(String username) {


        Query q = em.createQuery("from User where email=:email");
        q.setParameter("email", username);
        User user = (User) q.getSingleResult();
        if(user == null)
            return null;
        return user;
 
	}

    public List<User> getCustomList() {
        TypedQuery<User> q = em.createQuery("from User", User.class);
        return q.getResultList();
    }

    @Override
    public <T> List<String> getFieldsAvailableForFullTextSearch(Class<T> resultClass) {
        List<String> fieldList = new ArrayList<String>();
        if (resultClass == User.class) {
            fieldList.add("firstName");
            fieldList.add("lastName");
            fieldList.add("email");

        }
        return fieldList;
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
 
}