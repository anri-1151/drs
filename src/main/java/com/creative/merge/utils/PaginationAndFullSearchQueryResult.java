package com.creative.merge.utils;
import org.dozer.DozerBeanMapper;

import java.util.ArrayList;
import java.util.List;

public class PaginationAndFullSearchQueryResult<T> {
    private static DozerBeanMapper mapper = new DozerBeanMapper();
    private List<T> results;
    private Long maxPages;

    public List<T> getResults() {
        return results;
    }
    public void setResults(List<T> results) {
        this.results = results;
    }

    public Long getMaxPages() {
        return maxPages;
    }
    public void setMaxPages(Long maxPages) {
        this.maxPages = maxPages;
    }

    public <S> PaginationAndFullSearchQueryResult<S> transform(Class<S> clazz) {
        PaginationAndFullSearchQueryResult<S> result = new PaginationAndFullSearchQueryResult<S>();

        List<S> results = new ArrayList<S>();
        for (T item : this.results) {
            results.add(mapper.map(item, clazz));
        }

        result.setResults(results);
        result.setMaxPages(this.getMaxPages());

        return result;
    }
}