package com.creative.merge.utils;

public class RequestResponse {
	
	public static final RequestResponse SUCCESS = new RequestResponse(true);
	
	private Boolean isSuccess;
	
	private String errorMessage;
	
	private String exceptionMessage;

	private Integer code;
	
	public RequestResponse() {}
	
	public static RequestResponse createErrorResponse(String errorMessage, String exceptionMessage, int code) {
		RequestResponse result = new RequestResponse(false);
		
		result.setErrorMessage(errorMessage);
		result.setExceptionMessage(exceptionMessage);
		result.setCode(code);
		return result;
	}

	public static RequestResponse createErrorResponseWithCode(Integer errorCode) {
		RequestResponse result = new RequestResponse(false);
		result.setIsSuccess(false);
		result.setCode(errorCode);
		return result;
	}
	
	public RequestResponse(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public Boolean getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(Boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getExceptionMessage() {
		return exceptionMessage;
	}

	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

}