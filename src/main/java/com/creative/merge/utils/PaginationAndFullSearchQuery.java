package com.creative.merge.utils;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

@Repository
public abstract class PaginationAndFullSearchQuery {
    // Should be implemented in dao
    public abstract EntityManager getEntityManager();

    public final String TABLE_ALIAS = "AMF_Table";

    private Long lastQueryPageCount;

    // Should be implemented for objects which loaders will be in dao
    public abstract <T> List<String> getFieldsAvailableForFullTextSearch(Class<T> resultClass);

    // Should be implemented for objects which loaders will be in dao
    public  <T> Predicate getPDQExpression(Class<T> resultClass, Object... objects) {
        return null;
    }

    public <T> PaginationAndFullSearchQueryResult<T> getPaginatedResultList(Class<T> resultClass,
                                                                            String searchExpression,
                                                                            String sortField,
                                                                            boolean isAscending,
                                                                            int pageNumber,
                                                                            int pageSize,
                                                                            Object... objects) {
        Predicate pdqExpression = getPDQExpression(resultClass, objects);
        EntityManager entityManager = getEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(resultClass);
        Root<T> returnClassRoot = criteriaQuery.from(resultClass);
        returnClassRoot.alias(TABLE_ALIAS);


        // if seacrhExpression and fields we need to search is not empty, then build where expression
        if (!StringUtils.isEmpty(searchExpression) && !getFieldsAvailableForFullTextSearch(resultClass).isEmpty()) {
            List<String> fields = getFieldsAvailableForFullTextSearch(resultClass);
            Predicate predicate = null;

            for (String field : fields) {
                Predicate tmpPredicate = criteriaBuilder.like(getField(returnClassRoot, field).as(String.class), "%" + searchExpression + "%");
                predicate = predicate == null ? tmpPredicate : criteriaBuilder.or(predicate, tmpPredicate);
            }

            if (pdqExpression != null) {
                predicate = criteriaBuilder.and(predicate, pdqExpression);
            }

            criteriaQuery.where(predicate);
        } else {
            if (pdqExpression != null) {
                criteriaQuery.where(pdqExpression);
            }
        }

        PaginationAndFullSearchQueryResult<T> result = new PaginationAndFullSearchQueryResult<T>();

        CriteriaQuery<Long> countQuery = createCountQuery(criteriaQuery, resultClass);
        if (countQuery != null) {
            TypedQuery<Long> query = entityManager.createQuery(countQuery);
            Long rowCount = query.getSingleResult();

            result.setMaxPages((rowCount - 1) / pageSize + 1);
        }

        if (result.getMaxPages() > pageNumber) {
            // if sort field is not empty, then create order by expression
            if (!StringUtils.isEmpty(sortField)) {
                criteriaQuery.orderBy(
                        isAscending ? criteriaBuilder.asc(getField(returnClassRoot, sortField)) : criteriaBuilder.desc(getField(returnClassRoot, sortField))
                );
            }

            TypedQuery<T> query = entityManager.createQuery(criteriaQuery);
            query.setFirstResult(pageNumber * pageSize);
            query.setMaxResults(pageSize);

            result.setResults(query.getResultList());
        } else {
            result.setResults(new ArrayList<T>());
        }

        return result;
    }

    private <T> Path<String> getField(Root<T> root, String fieldName) {
        String[] fields = fieldName.split("\\.");
        String alias = TABLE_ALIAS;
        if (fields.length > 1) {
            Join<String, String> join = root.join(fields[0], JoinType.LEFT);
            alias += "X" + fields[0];
            join.alias(alias);
            for (int i=1; i<fields.length - 1; i++) {
                alias += "X" + fields[i];
                join = join.join(fields[i], JoinType.LEFT);
                join.alias(alias);
            }

            return join.get(fields[fields.length - 1]);
        } else {
            return root.get(fields[fields.length - 1]);
        }
    }

    private <T> CriteriaQuery<Long> createCountQuery(CriteriaQuery<T> criteriaQuery, Class<T> clazz) {
        CriteriaQuery<Long> countQuery = getEntityManager().getCriteriaBuilder().createQuery(Long.class);

        for (Root<?> root : criteriaQuery.getRoots()) {
            Root<?> dest = countQuery.from(root.getJavaType());
            dest.alias(root.getAlias());

            copyJoins(root, dest);
        }

        if (criteriaQuery.getRestriction() != null) {
            countQuery.where(criteriaQuery.getRestriction());
        }

        Root<T> root = null;

        for (Root<?> r : criteriaQuery.getRoots()) {
            if (clazz.equals(r.getJavaType())) {
                root = (Root<T>) r.as(clazz);
            }
        }

        Expression<Long> countExpression = getEntityManager().getCriteriaBuilder().count(root);

        return countQuery.select(countExpression);
    }

    private void copyJoins(From<?, ?> from, From<?, ?> to) {
        for (Join<?, ?> j : from.getJoins()) {
            Join<?, ?> toJoin = to.join(j.getAttribute().getName(), j.getJoinType());
            toJoin.alias(j.getAlias());

            copyJoins(j, toJoin);
        }
    }

    public Long getLastQueryPageCount() {
        return lastQueryPageCount;
    }
}