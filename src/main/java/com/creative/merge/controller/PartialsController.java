package com.creative.merge.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/templates" )
public class PartialsController {

    @RequestMapping("/getHomeTemplate" )
	public String getHomeTemplate() {
		return "home/main";
	}

}


