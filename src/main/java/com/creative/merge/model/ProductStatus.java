package com.creative.merge.model;

import javax.persistence.*;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: g_arc_000
 * Date: 12/5/15
 * Time: 5:10 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "PRODUCT_STATUS")
public class ProductStatus extends KingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "STATUS_NAME", nullable = false)
    private String statusName;
    
    @Column(name = "COLOR", nullable = false)
    private String color;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
    
    public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
}
