package com.creative.merge.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: g_arc_000
 * Date: 12/5/15
 * Time: 5:09 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "KEYWORD")
public class Keyword  extends KingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "KEYWORD_NAME", nullable = false)
    private String keywordName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKeywordName() {
        return keywordName;
    }

    public void setKeywordName(String keywordName) {
        this.keywordName = keywordName;
    }
}
