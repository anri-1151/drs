package com.creative.merge.model;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: g_arc_000
 * Date: 12/5/15
 * Time: 4:43 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "PRODUCT")
public class Product extends KingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "REQUEST_ID", nullable = false)
    private Request request;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name="PRODUCT_FACE_ID")
    private ProductFace productFace;

    @Column(name = "DATE", nullable = false)
    private Date date;

    @Column(name = "PRODUCT_TITLE", nullable = true)
    private String productTitle;

    @Column(name = "PRODUCT_SUB_TITLE", nullable = true)
    private String productSubTitle;

    @OneToMany(fetch = FetchType.LAZY)
    private Set<User> user = new HashSet<User>();


    @OneToMany(fetch = FetchType.LAZY)
    private Set<ProductContent> productContent = new HashSet<ProductContent>();

    @OneToMany(fetch = FetchType.LAZY)
    private Set<Country> countries = new HashSet<Country>();

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name="LANGUAGE_ID")
    private Language language;

    @OneToMany(fetch = FetchType.LAZY)
    private Set<Keyword> Keywords = new HashSet<Keyword>();

    @Column(name = "REFERANCE_COUNT", nullable = true)
    private Integer referanceCount;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name="PRODUCT_STATUS_ID")
    private ProductStatus productStatus;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name="DOCUMENT_ID")
    private Document document;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
    private Set<ProductAttachment> attachments = new HashSet<ProductAttachment>();

    @Column(name = "NOTE", nullable = true)
    private String note;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public ProductFace getProductFace() {
		return productFace;
	}

	public void setProductFace(ProductFace productFace) {
		this.productFace = productFace;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	public String getProductSubTitle() {
		return productSubTitle;
	}

	public void setProductSubTitle(String productSubTitle) {
		this.productSubTitle = productSubTitle;
	}

	public Set<User> getUser() {
		return user;
	}

	public void setUser(Set<User> user) {
		this.user = user;
	}

	public Set<ProductContent> getProductContent() {
		return productContent;
	}

	public void setProductContent(Set<ProductContent> productContent) {
		this.productContent = productContent;
	}

	public Set<Country> getCountries() {
		return countries;
	}

	public void setCountries(Set<Country> countries) {
		this.countries = countries;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public Set<Keyword> getKeywords() {
		return Keywords;
	}

	public void setKeywords(Set<Keyword> keywords) {
		Keywords = keywords;
	}

	public Integer getReferanceCount() {
		return referanceCount;
	}

	public void setReferanceCount(Integer referanceCount) {
		this.referanceCount = referanceCount;
	}

	public ProductStatus getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(ProductStatus productStatus) {
		this.productStatus = productStatus;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public Set<ProductAttachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(Set<ProductAttachment> attachments) {
		this.attachments = attachments;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	
}
