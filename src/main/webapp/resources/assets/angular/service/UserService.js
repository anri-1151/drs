angular.module('drs').service('UserService',function AppService($http) {
    this.addEditUser = function (user) {
        return $http({
            url: 'user/saveUser',
            method: 'POST',
            data: user
        }).then(function (response) {
            return response.data;
        });
    };
    
    this.deleteUser = function (id) {
        return $http({
            url: 'user/delete',
            method: 'GET',
            params : {
                id : id
            }
        }).then(function (response) {
            return response.data;
        });
    };

    this.getUserList = function () {
        return $http({
            url: 'user/customList',
            method: 'GET'
        }).then(function (response) {
            return response.data;
        });
    };
});