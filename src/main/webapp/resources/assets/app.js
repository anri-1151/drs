'use strict';

// Declare app level module which depends on views, and components
var skeletonApp = angular.module('drs', ['ui.router','ui.bootstrap', 'ngToast']);
skeletonApp.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/');
    
    $stateProvider
        .state('/', {
            url: '/',
            templateUrl: 'templates/getHomeTemplate',
            controller: 'MainController'
        })
        .state('users', {
        	url: '/users',
            templateUrl: 'user/getTemplate',
            controller: 'UserController'
        });
        
});

